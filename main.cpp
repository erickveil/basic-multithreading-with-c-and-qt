#include <QCoreApplication>
#include <QThread>

#include "counter.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QThread thread;
    int maxCount = 10;
    Counter threadCounter(maxCount);
    threadCounter.moveToThread(&thread);

    QObject::connect(&thread, &QThread::started,
                     &threadCounter, &Counter::doCounting);

    qDebug() << "Main thread ID: " << QThread::currentThreadId();

    thread.start();

    Counter mainCounter(maxCount);
    mainCounter.doCounting();

    return a.exec();
}
