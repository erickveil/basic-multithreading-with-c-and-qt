#ifndef COUNTER_H
#define COUNTER_H

#include <QDebug>
#include <QObject>
#include <QString>
#include <QThread>
#include <QTime>

class Counter : public QObject
{
    Q_OBJECT
public:
    explicit Counter(int maxCount, QObject *parent = nullptr);

    int _maxCount;

public slots:
    void doCounting();
};

#endif // COUNTER_H
