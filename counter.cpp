#include "counter.h"

Counter::Counter(int maxCount, QObject *parent)
    : QObject{parent}
{
    _maxCount = maxCount;
}

void Counter::doCounting()
{
    for (int i = 0; i < _maxCount; ++i) {
        QString time = QTime::currentTime().toString("hh:mm:ss:zzz");
        qDebug() << "Time: " << time << " | "
                 << "Thread: " << QThread::currentThreadId() << " | "
                 << "Count: " << i;
        QThread::sleep(1);
    }
}
